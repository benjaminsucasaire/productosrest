package com.gestion.productos.comgestionproductos.repositorio;

import com.gestion.productos.comgestionproductos.modelo.Producto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductoRepositorio extends JpaRepository<Producto,Integer> {

}
